<?php
class Author_model extends CI_Model{
    function all()
    {
      return $authors = $this->db->get('authors')->result_array(); //select * from books
    }

    function create($formArray)
    {
      $this->db->insert('authors',$formArray); // insert into books(book name,category,price)
    }

    function getAuthors($authorsId)
    {
      $this->db->where('author_id',$authorsId);
      return $authors = $this->db->get('authors')->row_array();
    }

    function update($authorsId, $formArray)
    {
          $this->db->where('author_id',$authorsId);
          $this->db->update('authors',$formArray); //Update users SET book_name=?, category=?, price=?, where book_id=?
    }

    function delete($authorsId)
    {
          $this->db->where('author_id',$authorsId);
          $this->db->delete('authors'); //Delete users SET book_name=?, category=?, price=?, where book_id=?
    }
}
?>
