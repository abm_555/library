<?php
class Issue_Model extends CI_Model{
    function all()
    {
      return $books = $this->db->get('issues')->result_array(); //select * from books
    }
    //
     function create($formArray)
    {
      $this->db->insert('issues',$formArray); // insert into books(book name,category,price)
    }

 	function getIssues($issuesId)
    {
      $this->db->where('issue_id',$issuesId);
      return $issues = $this->db->get('books')->row_array();
    }
    function delete($issueId)
    {
          $this->db->where('issue_id',$issuesId);
          $this->db->delete('issues'); //Delete users SET book_name=?, category=?, price=?, where book_id=?
    }
    public function getBook(){
        $books = $this->db->get('books');
        if($books->num_rows() > 0){
        return $books->result();
      }

    }
    public function getUser(){
        $users = $this->db->get('users');
        if($users->num_rows() > 0){
        return $users->result();
      }

    }
    public function getBookRecord($issuesId){
    //  $this->db->select(['books.auth_id','books.book_name','categorys.category_id','authors.author_id']);
      $this->db->select('*');
      $this->db->from('books');
      $this->db->from('users');
      $this->db->join('issues','issues.book_name = books.book_name', 'issues.student_name = users.first_name');
      $this->db->where(['issues.issue_id' =>$issuesId]);
      $issues = $this->db->get();
      return $issues->row();
    }
/*
    public function updateBook($formArray,$booksId){
        return $this->db->where('book_id',$booksId)
                        ->update('books',$formArray);
    }*/
}
?>
