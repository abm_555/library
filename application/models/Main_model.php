<?php
class Main_model extends CI_Model{
    function all()
    {
      return $books = $this->db->get('books')->result_array(); //select * from books
    }
    //
     function create($formArray)
    {
      $this->db->insert('books',$formArray); // insert into books(book name,category,price)
    }

 	function getBooks($booksId)
    {
      $this->db->where('book_id',$booksId);
      return $books = $this->db->get('books')->row_array();
    }

/*    function update($booksId, $formArray)
    {
          $this->db->where('books_id',$booksId);
          $this->db->update('books',$formArray); //Update users SET book_name=?, category=?, price=?, where book_id=?
    }
*/
    function delete($booksId)
    {
          $this->db->where('book_id',$booksId);
          $this->db->delete('books'); //Delete users SET book_name=?, category=?, price=?, where book_id=?
    }
    public function getCategory(){
        $cates = $this->db->get('categorys');
        if($cates->num_rows() > 0){
        return $cates->result();
      }

    }
    public function getAuthor(){
        $auths = $this->db->get('authors');
        if($auths->num_rows() > 0){
        return $auths->result();
      }

    }
    public function getBookRecord($booksId){
    //  $this->db->select(['books.auth_id','books.book_name','categorys.category_id','authors.author_id']);
      $this->db->select('*');
      $this->db->from('authors','categorys');
      $this->db->from('categorys');
      $this->db->join('books','books.auth_id = authors.author_id', 'books.cat_id = categorys.category_id');
      $this->db->where(['books.book_id' =>$booksId]);
      $books = $this->db->get();
      return $books->row();
    }

    public function updateBook($formArray,$booksId){
        return $this->db->where('book_id',$booksId)
                        ->update('books',$formArray);
    }
}
?>
