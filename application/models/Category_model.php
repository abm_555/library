<?php
class Category_model extends CI_Model{
    function all()
    {
      return $categorys = $this->db->get('categorys')->result_array(); //select * from books
    }

    function create($formArray)
    {
      $this->db->insert('categorys',$formArray); // insert into books(book name,category,price)
    }

    function getCategorys($categorysId)
    {
      $this->db->where('category_id',$categorysId);
      return $categorys = $this->db->get('categorys')->row_array();
    }

    function update($categorysId, $formArray)
    {
          $this->db->where('category_id',$categorysId);
          $this->db->update('categorys',$formArray); //Update users SET book_name=?, category=?, price=?, where book_id=?
    }

    function delete($categorysId)
    {
          $this->db->where('category_id',$categorysId);
          $this->db->delete('categorys'); //Delete users SET book_name=?, category=?, price=?, where book_id=?
    }
}
?>
