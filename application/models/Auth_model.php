<?php
class Auth_model extends CI_Model{
	/*This function will save a user record in db*/
	public function create($formArray){
		$this->db->insert('admin',$formArray);
	}
	public function checkUser($email){
		$this->db->where('email',$email);
		return $row = $this->db->get('admin')->row_array();
	}
	/*check user authorization*/
	function authorized(){
		$admin = $this->session->userdata('admin');
		if(!empty($admin)){
			return true;
		}else{
			return false;
		}
	}
}
?>