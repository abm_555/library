<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_Profile_Model extends CI_Model{

public function getprofile($userid){
	$query=$this->db->select('first_name,last_name,email,phone,regDate')
                ->where('id',$userid)
                ->from('users')
                ->get();
  return $query->row();
}

public function update_profile($first_name,$last_name,$phone,$userid){
$data = array(
               'first_name' =>$first_name,
               'last_name' => $last_name,
               'phone' => $phone
            );

$sql_query=$this->db->where('id', $userid)
                ->update('users', $data);


}


}
