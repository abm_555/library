<?php
class Uview_model extends CI_Model{
    function all()
    {
      return $users = $this->db->get('users')->result_array(); //select * from books
    }

    function create($formArray)
    {
      $this->db->insert('users',$formArray); // insert into books(book name,category,price)
    }

    function getUsers($usersId)
    {
      $this->db->where('id',$usersId);
      return $users = $this->db->get('users')->row_array();
    }

    function update($usersId, $formArray)
    {
          $this->db->where('id',$usersId);
          $this->db->update('users',$formArray); //Update users SET book_name=?, category=?, price=?, where book_id=?
    }

    function delete($usersId)
    {
          $this->db->where('id',$usersId);
          $this->db->delete('users'); //Delete users SET book_name=?, category=?, price=?, where book_id=?
    }

}
?>
