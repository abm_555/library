<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Home Page</title>
      <link rel="stylesheet"  href="<?php echo base_url().'assets/vendor/bootstrap/css/bootstrap.min.css'?>">
      <link rel="stylesheet"  href="<?php echo base_url().'assets/vendor/fontawesome-free/css/all.min.css'?>">
      <link rel="stylesheet"  href="<?php echo base_url().'assets/vendor/datatables/dataTables.bootstrap4.css'?>">
      <link rel="stylesheet"  href="<?php echo base_url().'assets/css/sb-admin.css'?>">
   </head>
   <body id="page-top">
      <div id="wrapper">
         <div id="content-wrapper">
            <div class="container-fluid">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                     <a>Welcome</a>
                  </li>
               </ol>
               <h1 class="text-center">Library Management System</h1>
               <hr>
               <div class="row">
                  <div class="col-xl-3 col-sm-6 mb-3">
                     &nbsp;
                  </div>
                  <div class="col-xl-3 col-sm-6 mb-3">
                     <div class="card text-white bg-primary o-hidden h-100">
                        <div class="card-body">
                           <div class="card-body-icon">
                              <i class="fas fa-fw fa-users"></i>
                           </div>
                           <div class="mr-5">User Login</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url('index.php/User/login'); ?>">
                        <span class="float-left">Click Here</span>
                        <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                        </span>
                        </a>
                     </div>
                  </div>
                  <div class="col-xl-3 col-sm-6 mb-3">
                     <div class="card text-white bg-warning o-hidden h-100">
                        <div class="card-body">
                           <div class="card-body-icon">
                              <i class="fas fa-fw fa-list"></i>
                           </div>
                           <div class="mr-5">Admin Login</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url('index.php/admin/login'); ?>">
                        <span class="float-left">Click Here</span>
                        <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                        </span>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/vendor/chart.js/Chart.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/vendor/datatables/jquery.dataTables.js'); ?>"></script>
      <script src="<?php echo base_url('assets/vendor/datatables/dataTables.bootstrap4.js'); ?>"></script>
      <script src="<?php echo base_url('assets/js/sb-admin.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/js/demo/datatables-demo.js'); ?>"></script>
      <script src="<?php echo base_url('assets/js/demo/chart-area-demo.js'); ?>"></script>
   </body>
</html>
