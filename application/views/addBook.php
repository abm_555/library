 <?php $this->load->view('admin/header');?>
<html>
<div class="container">
		<h3>ADD</h3>
		<hr>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-3 control-label">Username</label>
					<div class="col-md-9">
						<?php echo form_input(['name'=>'username', 'class'=>'form-control', 'plcaeholder'=>'Username','value'=>set_value('username')]);?>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<?php echo form_error('username','<div class="text-danger">','</div>');?>
		</div>
	</div>

<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-3 control-label">Password</label>
					<div class="col-md-9">
						<?php echo form_password(['name' => 'password', 'class'=>'form-control', 'plcaeholder'=>'Password']);?>

				</div>
			</div>
		</div>
		<div class="col-md-6">
			<?php echo form_error('password','<div class="text-danger">','</div>');?>
		</div>
	</div>

<button type="submit" class="btn btn-primary">ADD</button>
<?php echo anchor("welcome", "BACK", ['class'=>'btn btn-primary']);?>
</div>