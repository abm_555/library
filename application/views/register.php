<html>
   <head>
      <title>Register</title>
      <link rel="stylesheet"  href="<?php echo base_url().'assets/css/bootstrap.css'?>">
       <link rel="stylesheet"  href="<?php echo base_url().'assets/css/style.css'?>">
   </head>
   <body>
      <div class="container">
      	<?php
      		$msg = $this->session->flashdata('msg');
      		if($msg !=""){
      			echo "<div class='alert alert-success'>'.$msg.'</div>";
      		}
      	?>
         <div class="col-md-6">
            <div class="card mt-4">
               <div class="card-header">
                  Register
               </div>
               <form action="<?php echo base_url().'index.php/Admin/register'?>" name="registerForm" id="registerForm" method="post">
               <div class="card-body register">
                  <h5 class="card-title">Please fill your details</h5>

                <div class="form-group">
                	<label for="name">Email</label>
                	<input type="text" name="email" id="email" value="<?php echo set_value('email')?>" class="form-control <?php echo (form_error('email') !="") ? 'is-invalid': ''; ?>" placeholder="Email">
                	 <p class="invalid-feedback"><?php echo form_error('email');?></p>
                </div>
                <div class="form-group">
                	<label for="name">Password</label>
                	<input type="password" name="password" id="password" value="<?php echo set_value('password')?>" class="form-control <?php echo (form_error('password') !="") ? 'is-invalid': ''; ?>" placeholder="Password">
                	 <p class="invalid-feedback"><?php echo form_error('password');?></p>
                </div>

                <div class="form-group">
                	<button class="btn btn-block btn-primary">REGISTER NOW</button>
                  <br>
                  <a class="d-block small" href="<?php echo site_url('index.php/Admin/login'); ?>">Back to Login</a>
                </div>
               </div>
               </form>
            </div>
         </div>
      </div>
   </body>
</html>
