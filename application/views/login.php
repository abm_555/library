 <html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <meta http-equiv="X-UA-Compatible"content="ie=edge">
      <title>Login</title>
      <link rel="stylesheet"  href="<?php echo base_url().'assets/css/bootstrap.css'?>">
      <link rel="stylesheet"  href="<?php echo base_url().'assets/css/style.css'?>">
      <link rel="stylesheet"  href="<?php echo base_url().'assets/css/login.css'?>">
      <link rel="stylesheet"  href="<?php echo base_url().'assets/vendor/fontawesome-free/css/all.min.css'?>">
      <link rel="stylesheet"  href="<?php echo base_url().'assets/vendor/datatables/dataTables.bootstrap4.css'?>">
      <link rel="stylesheet"  href="<?php echo base_url().'assets/css/sb-admin.css'?>">
   </head>
   <body>
      <form class="form-signin" action="<?php echo base_url().'index.php/admin/login';?>" method="post" name="frm">
        <?php
          $msg =$this->session->flashdata('msg');
        if($msg !=""){
        ?>
        <div class="alert alert-danger">
            <?php echo $msg;?>
        </div>
        <?php
      }
      ?>
      <div class="card-body">
         <div class="card-body-icon">
            <i class="fas fa-fw fa-lock"></i>
         </div>
      </div>
         <h1 class="h3 mb-3 font-weight-normal text-center">ADMIN LOGIN</h1>

         <label for="email" class="sr-only">Email address</label>
         <input type="text" name="email" id="email" value="<?php echo set_value('email')?>" class="form-control <?php echo (form_error('email') !="") ? 'is-invalid': ''; ?>" placeholder="Email">
         <p class="invalid-feedback"><?php echo strip_tags(form_error('email'));?></p>
         <label for="password" class="sr-only">Password</label>
         <input type="password" id="password" name="password" class="form-control <?php echo (form_error('email') !="") ? 'is-invalid': ''; ?>" placeholder="Password" value="<?php echo set_value('password')?>">
         <p class="invalid-feedback"><?php echo strip_tags(form_error('password'));?></p>
         <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
         <div class="text-center">
         <a class="d-block small mt-3" href="<?php echo site_url('index.php/admin/register'); ?>">Register an Account</a>
         <a class="d-block small" href="<?php echo site_url('index.php/Welcome/index'); ?>">Back to Home page</a>

          </div>
      </form>
   </body>
</html>
