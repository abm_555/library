<!DOCTYPE html>
<html lang="en">
  <head>
<title>Admin - Dashboard</title>
<link rel="stylesheet"  href="<?php echo base_url().'assets/vendor/bootstrap/css/bootstrap.min.css'?>">
<link rel="stylesheet"  href="<?php echo base_url().'assets/vendor/fontawesome-free/css/all.min.css'?>">
<link rel="stylesheet"  href="<?php echo base_url().'assets/vendor/datatables/dataTables.bootstrap4.css'?>">
<link rel="stylesheet"  href="<?php echo base_url().'assets/css/sb-admin.css'?>">
</head>
<body id="page-top">
       <?php include APPPATH.'views/admin/includes/header.php';?>
       <div id="wrapper">
                   <?php include APPPATH.'views/admin/includes/sidebar.php';?>
             <div id="content-wrapper">

               <div class="container-fluid">
                 <ol class="breadcrumb">
                   <li class="breadcrumb-item">
                     <a href="#">Dashboard</a>
                   </li>
                   <li class="breadcrumb-item active">Overview</li>
                 </ol>
                 <div class="row">
                   <div class="col-xl-4 col-sm-6 mb-3">
                     <div class="card text-white bg-primary o-hidden h-100">
                       <div class="card-body">
                         <div class="card-body-icon">
                           <i class="fas fa-fw fa-comments"></i>
                         </div>
                         <div class="mr-5"><?php echo htmlentities($tcount);?>-Users</div>
                       </div>
                       <a class="card-footer text-white clearfix small z-1" href="http://localhost/library/index.php/Uview/index">
                         <span class="float-left ">Total Registered Users</span>
                         <span class="float-right">
                           <i class="fas fa-angle-right " ></i>
                           
                         </span>
                       </a>
                     </div>
                   </div>

                  <div class="col-xl-4 col-sm-6 mb-3">
                     <div class="card text-white bg-warning o-hidden h-100">
                       <div class="card-body">
                         <div class="card-body-icon">
                           <i class="fas fa-fw fa-book"></i>
                         </div>
                         <div class="mr-5"><?php echo htmlentities($bcount);?>-Books in Library</div>
                       </div>
                       <a class="card-footer text-white clearfix small z-1" href="http://localhost/library/index.php/Main/index">
                         <span class="float-left">Total Available Books</span>
                         <span class="float-right">
                           <i class="fas fa-angle-right"></i>
                         </span>
                       </a>
                     </div>
                   </div>

                   <div class="col-xl-4 col-sm-6 mb-3">
                     <div class="card text-white bg-success o-hidden h-100">
                       <div class="card-body">
                         <div class="card-body-icon">
                           <i class="fas fa-fw fa-shopping-cart"></i>
                         </div>
                         <div class="mr-5"><?php echo htmlentities($icount);?>-Books-Issued</div>
                       </div>
                       <a class="card-footer text-white clearfix small z-1" href="http://localhost/library/index.php/Issue/index">
                         <span class="float-left">Total Books Issued</span>
                         <span class="float-right">
                           <i class="fas fa-angle-right"></i>
                         </span>
                       </a>
                     </div>
                   </div>

                 </div>
               </div>
               <?php include APPPATH.'views/admin/includes/footer.php';?>
             </div>
           </div>
           <a class="scroll-to-top rounded" href="#page-top">
             <i class="fas fa-angle-up"></i>
           </a>


           <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
           <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
           <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>
           <script src="<?php echo base_url('assets/vendor/chart.js/Chart.min.js'); ?>"></script>
           <script src="<?php echo base_url('assets/vendor/datatables/jquery.dataTables.js'); ?>"></script>
           <script src="<?php echo base_url('assets/vendor/datatables/dataTables.bootstrap4.js'); ?>"></script>
           <script src="<?php echo base_url('assets/js/sb-admin.min.js'); ?>"></script>
           <script src="<?php echo base_url('assets/js/demo/datatables-demo.js'); ?>"></script>
           <script src="<?php echo base_url('assets/js/demo/chart-area-demo.js'); ?>"></script>
         </body>

       </html>
