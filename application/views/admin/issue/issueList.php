<?php $this->load->view('admin/basic');?>
<html>
 <head>
 	<title>LMS-ADMIN</title>
 	 <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
 </head>
 <body>
 	<div class="navbar navbar-dark bg-dark">
 	</div>
	<div class="container" style="padding-top: 10px;">
		<div class="row">
			<div class="col-md-12">
				 <?php
				 $success = $this->session->flashdata('success');
				 if($success != ""){
				 ?>
				 <div class="alert alert-success"><?php echo $success;?></div>
				 <?php
				}
				?>
				<?php
				 $failure = $this->session->flashdata('failure');
				 if($failure != ""){
				 ?>
				 <div class="alert alert-success"><?php echo $failure;?></div>
				 <?php
				}
				?>
			</div>
		</div>
		 <div class="row">
		 	<div class="col-md-8">
		 		<div class="row">
		 			<div class="col-6"><h3>MANAGE-ISSUES</h3></div>
					<div class="col-6 text-right">
						<a href="<?php echo base_url().'index.php/issue/createIssue';?>" class="btn btn-primary"><i class="fas fa-plus"></i> ADD</a>
					</div>
		 		</div>
		 		<hr>
		 	</div>
		 </div>

		<div class="row">

			<div class="col-md-8">
				<table class="table table-striped">
					<tr>
						<th>Issue ID</th>
						<th>Book Name</th>
						<th>Student Name</th>
						<th>Delete</th>

					</tr>

					<?php if(!empty($issues)) { foreach($issues as $issues) {?>
						<tr>
							<td><?php echo $issues['issue_id']?></td>
							<td><?php echo $issues['book_name']?></td>
							<td><?php echo $issues['student_name']?></td>
							<td>
								<a href="<?php echo base_url().'index.php/Issue/delete/'.$issues['issue_id']?>" class="btn btn-danger btn-sm"> <i class="far fa-trash-alt"></i> Delete</a>
							</td>
						</tr>
					<?php }} else { ?>
						<tr>
							<td colspan="5">Records not found</td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</body>
 </html>
