<?php $this->load->view('admin/basic');?>
<html>
   <head>
      <title>Book-Issue</title>
      <link rel="stylesheet"  href="<?php echo base_url().'assets/css/bootstrap.css'?>">
       <link rel="stylesheet"  href="<?php echo base_url().'assets/css/style.css'?>">
   </head>
   <body>
      <div class="container">
      	<?php
      		$msg = $this->session->flashdata('msg');
      		if($msg !=""){
      			echo "<div class='alert alert-success'>'.$msg.'</div>";
      		}
      	?>
         <div class="col-md-6">
            <div class="card mt-4">
               <div class="card-header">
                  ISSUE BOOK
               </div>
              <form action="<?php echo base_url().'index.php/Issue/createIssue'?>" name="createIssue" id="createIssue" method="post">
               <div class="card-body book">
                  <h5 class="card-title">Please Enter the details</h5>

				<div class="form-group">
                  	<label class="col-md-3">Book Name</label>
                  <select class="col-lg-9" name="book_name">
                    <option value="">Select Book</option>
                      <?php if(count($books)):?>
                      <?php  foreach($books as $books):?>
                    <option value=<?php echo $books->book_name?>><?php echo $books->book_name?></option>
                  <?php endforeach;?>
                <?php endif;?>
                </select>
                <p class="invalid-feedback"><?php echo form_error('cat_id');?></p>
                </div>

                <div class="form-group">
                  	<label class="col-md-3">Student Name</label>
                  <select class="col-lg-9" name="student_name">
                    <option value="">Select Student</option>
                      <?php if(count($users)):?>
                      <?php  foreach($users as $users):?>
                    <option value=<?php echo $users->first_name?>><?php echo $users->first_name?></option>
                  <?php endforeach;?>
                <?php endif;?>
                </select>
                <p class="invalid-feedback"><?php echo form_error('auth_id');?></p>
                </div>

                <div class="form-group">
                	<button class="btn btn-primary">SUBMIT</button>
                  <a href="<?php echo base_url().'index.php/Issue/index'?>" class="btn-secondary btn">Cancel</a>
                </div>
               </div>
               </form>
            </div>
         </div>
      </div>
   </body>
</html>
