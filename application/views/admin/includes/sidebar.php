<ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('index.php/Dashboard/index'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('index.php/Category/index'); ?>">
            <i class="fas fa-fw fa-compass"></i>
            <span>Categorys</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('index.php/Author/index'); ?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Author</span></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('index.php/Main/index'); ?>">
            <i class="fas fa-fw fa-book"></i>
            <span>Books</span></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('index.php/Issue/index'); ?>">
            <i class="fas fa-fw fa-book"></i>
            <span>Book-Issues</span></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('index.php/Uview/index'); ?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('index.php/Dashboard/logout'); ?>">
            <i class="fas fa-sign-out-alt"></i>
            <span>Logout</span></a>
        </li>

      </ul>
