<?php $this->load->view('admin/basic');?>
<html>
 <head>
 	<title>LMS-ADMIN</title>
 	 <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
 </head>
 <body>
 	<div class="navbar navbar-dark bg-dark">
</div>
	<div class="container" style="padding-top: 10px;">
		<h3>EDIT USER</h3>
		<hr>
		<form method="post" name="createUser" action="<?php echo base_url().'index.php/Uview/edit/'.$users['id'];?>">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>First Name</label>
					<input type="text" name="first_name" id="first_name" value="<?php echo set_value('first_name',$users['first_name']);?>" class="form-control">
					<?php echo form_error('first_name')?>
				</div>

        <div class="form-group">
          <label>Last Name</label>
          <input type="text" name="last_name" id="last_name" value="<?php echo set_value('last_name',$users['last_name']);?>" class="form-control">
          <?php echo form_error('last_name')?>
        </div>

        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="email" value="<?php echo set_value('email',$users['email']);?>" class="form-control">
          <?php echo form_error('email')?>
        </div>

        <div class="form-group">
          <label>Password</label>
          <input type="text" name="password" id="password" value="<?php echo set_value('password',$users['password']);?>" class="form-control">
          <?php echo form_error('password')?>
        </div>

        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" id="phone" value="<?php echo set_value('phone',$users['phone']);?>" class="form-control">
          <?php echo form_error('password')?>
        </div>

				<div class="form-group">
					<button class="btn btn-primary">Update</button>
					<a href="<?php echo base_url().'index.php/Uview/index'?>" class="btn-secondary btn">Cancel</a>
				</div>


			</div>

		</div>
		</form>
	</div>
</body>
 </html>
