<?php $this->load->view('admin/basic');?>
<html>
 <head>
 	<title>LMS-ADMIN</title>
 	 <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
 </head>
 <body>
 	<div class="navbar navbar-dark bg-dark">
</div>
	<div class="container" style="padding-top: 10px;">
    <?php echo form_open("index.php/Main/modifyBooks/{$books->book_id}",['class' => 'form-horizontal']);?>
    <h3>EDIT BOOKS</h3>
		<hr>

	 <div class="card-body book">
                <div class="form-group">
                <label for="name">Book Name</label>
                <input type="text" name="book_name" id="book_name" value="<?php echo set_value('book_name',$books->book_name)?>" class="col-md-11 form-control <?php echo (form_error('book_name') !="") ? 'is-invalid': ''; ?>" placeholder="Book Name">
                <p class="invalid-feedback"><?php echo form_error('book_name');?></p>
                </div>

				<div class="form-group">
                  	<label class="col-md-2">Category Name</label>
                  <select class="col-lg-9" name="cat_id">
                    <option value=<?php echo $books->cat_id;?>><?php echo $books->category_name;?></option>
                      <?php if(count($cates)):?>
                      <?php  foreach($cates as $cates):?>
                    <option value=<?php echo $cates->category_id?>><?php echo $cates->category_name?></option>
                  <?php endforeach;?>
                <?php endif;?>
                </select>
                <p class="invalid-feedback"><?php echo form_error('cat_id');?></p>
                </div>

                <div class="form-group">
                  	<label class="col-md-2">Author Name</label>
                  <select class="col-lg-9" name="auth_id">
                    <option value=<?php echo $books->auth_id;?>><?php echo $books->author_name;?></option>
                      <?php if(count($auths)):?>
                      <?php  foreach($auths as $auths):?>
                    <option value=<?php echo $auths->author_id?>><?php echo $auths->author_name?></option>
                  <?php endforeach;?>
                <?php endif;?>
                </select>
                <p class="invalid-feedback"><?php echo form_error('auth_id');?></p>
                </div>

                <div class="form-group">
                <button type="submit" class="btn btn-primary">SUBMIT</button>
                <?php echo anchor("index.php/Main/index/{$books->book_id}","BACK",['class' =>'btn btn-primary']);?>

                </div>
               </div>
		</form>
	</div>
</body>
 </html>
