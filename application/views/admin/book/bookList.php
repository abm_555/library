<?php $this->load->view('admin/basic');?>
<html>
 <head>
 	<title>LMS-ADMIN</title>
 	 <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
 </head>
 <body>
 	<div class="navbar navbar-dark bg-dark">
</div>
	<div class="container" style="padding-top: 10px;">
		<div class="row">
			<div class="col-md-12">
				 <?php
				 $success = $this->session->flashdata('success');
				 if($success != ""){
				 ?>
				 <div class="alert alert-success"><?php echo $success;?></div>
				 <?php
				}
				?>
				<?php
				 $failure = $this->session->flashdata('failure');
				 if($failure != ""){
				 ?>
				 <div class="alert alert-success"><?php echo $failure;?></div>
				 <?php
				}
				?>
			</div>
		</div>
		 <div class="row">
		 	<div class="col-md-8">
		 		<div class="row">
		 			<div class="col-6"><h3>MANAGE-BOOKS</h3></div>
					<div class="col-6 text-right">
						<a href="<?php echo base_url().'index.php/Main/createBook';?>" class="btn btn-primary"><i class="fas fa-plus"></i> ADD</a>
					</div>
		 		</div>
		 		<hr>
		 	</div>
		 </div>

		<div class="row">

			<div class="col-md-8">
				<table class="table table-striped">
					<tr>
						<th>Book ID</th>
						<th>Book Name</th>
						<th>Category ID</th>
						<th>Author ID</th>
						<th>Edit</th>
						<th>Delete</th>

					</tr>

					<?php if(!empty($books)) { foreach($books as $books) {?>
						<tr>
							<td><?php echo $books['book_id']?></td>
							<td><?php echo $books['book_name']?></td>
							<td><?php echo $books['cat_id']?></td>
							<td><?php echo $books['auth_id']?></td>
							<td>
								<a href="<?php echo base_url().'index.php/Main/editBooks/'.$books['book_id']?>" class="btn btn-primary btn-sm"><i class="far fa-edit"></i> Edit</a>
							</td>
							<td>
								<a href="<?php echo base_url().'index.php/Main/delete/'.$books['book_id']?>" class="btn btn-danger btn-sm"> <i class="far fa-trash-alt"></i> Delete</a>
							</td>
						</tr>
					<?php }} else { ?>
						<tr>
							<td colspan="5">Records not found</td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</body>
 </html>
