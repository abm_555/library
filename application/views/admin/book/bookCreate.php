<?php $this->load->view('admin/basic');?>
<html>
   <head>
      <title>Add Book</title>
      <link rel="stylesheet"  href="<?php echo base_url().'assets/css/bootstrap.css'?>">
       <link rel="stylesheet"  href="<?php echo base_url().'assets/css/style.css'?>">
   </head>
   <body>
      <div class="container">
      	<?php
      		$msg = $this->session->flashdata('msg');
      		if($msg !=""){
      			echo "<div class='alert alert-success'>'.$msg.'</div>";
      		}
      	?>
         <div class="col-md-6">
            <div class="card mt-4">
               <div class="card-header">
                  ADD-BOOK
               </div>
              <form action="<?php echo base_url().'index.php/Main/createBook'?>" name="createBook" id="createBook" method="post">
               <div class="card-body book">
                  <h5 class="card-title">Please Enter the details</h5>

                <div class="form-group">
                <label for="name">Book Name</label>
                <input type="text" name="book_name" id="book_name" value="<?php echo set_value('book_name')?>" class="form-control <?php echo (form_error('book_name') !="") ? 'is-invalid': ''; ?>" placeholder="Book Name">
                <p class="invalid-feedback"><?php echo form_error('book_name');?></p>
                </div>

				<div class="form-group">
                  	<label class="col-md-3">Category Name</label>
                  <select class="col-lg-9" name="cat_id">
                    <option value="">Select Category</option>
                      <?php if(count($cates)):?>
                      <?php  foreach($cates as $cates):?>
                    <option value=<?php echo $cates->category_id?>><?php echo $cates->category_name?></option>
                  <?php endforeach;?>
                <?php endif;?>
                </select>
                <p class="invalid-feedback"><?php echo form_error('cat_id');?></p>
                </div>

                <div class="form-group">
                  	<label class="col-md-3">Author Name</label>
                  <select class="col-lg-9" name="auth_id">
                    <option value="">Select Author</option>
                      <?php if(count($auths)):?>
                      <?php  foreach($auths as $auths):?>
                    <option value=<?php echo $auths->author_id?>><?php echo $auths->author_name?></option>
                  <?php endforeach;?>
                <?php endif;?>
                </select>
                <p class="invalid-feedback"><?php echo form_error('auth_id');?></p>
                </div>

                <div class="form-group">
                	<button class="btn btn-primary">SUBMIT</button>
                  <a href="<?php echo base_url().'index.php/Main/index'?>" class="btn-secondary btn">Cancel</a>
                </div>
               </div>
               </form>
            </div>
         </div>
      </div>
   </body>
</html>
