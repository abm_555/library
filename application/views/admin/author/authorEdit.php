<?php $this->load->view('admin/basic');?>
<html>
   <head>
      <title>LMS-ADMIN</title>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
   </head>
   <body>
      <div class="container" style="padding-top: 10px;">
         <h3>EDIT AUTHOR</h3>
         <hr>
         <form method="post" name="createAuthor" action="<?php echo base_url().'index.php/Author/edit/'.$authors['author_id'];?>">
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <label>Author Name</label>
                     <input type="text" name="author_name" id="author_name" value="<?php echo set_value('author_name',$authors['author_name']);?>" class="form-control">
                     <?php echo form_error('author_name')?>
                  </div>
                  <div class="form-group">
                     <button class="btn btn-primary">Update</button>
                     <a href="<?php echo base_url().'index.php/Author/index'?>" class="btn-secondary btn">Cancel</a>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </body>
</html>
