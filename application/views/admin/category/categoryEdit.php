<?php $this->load->view('admin/basic');?>
<html>
 <head>
 	<title>LMS-ADMIN</title>
 	 <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
 </head>
 <body>
 	<div class="navbar navbar-dark bg-dark">
</div>
	<div class="container" style="padding-top: 10px;">
		<h3>EDIT CATEGORY</h3>
		<hr>
		<form method="post" name="createCategory" action="<?php echo base_url().'index.php/Category/edit/'.$categorys['category_id'];?>">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Category Name</label>
					<input type="text" name="category_name" id="category_name" value="<?php echo set_value('category_name',$categorys['category_name']);?>" class="form-control">
					<?php echo form_error('category_name')?>
				</div>

        <div class="custom-control custom-radio float-left">
            <input class="custom-control-input" value="1" type="radio" id="statusActive" name="status" checked="">
            <label for="statusActive" class="custom-control-label">Active</label>
        </div>

        <div class="custom-control custom-radio float-left ml-3">
              <input class="custom-control-input" value="0" type="radio" id="statusInactive" name="status">
              <label for="statusInactive" class="custom-control-label">Inactive</label>
        </div>

				<div class="form-group">
					<button class="btn btn-primary">Update</button>
					<a href="<?php echo base_url().'index.php/Category/index'?>" class="btn-secondary btn">Cancel</a>
				</div>
			</div>

		</div>
		</form>
	</div>
</body>
 </html>
