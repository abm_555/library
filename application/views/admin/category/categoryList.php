<?php $this->load->view('admin/basic');?>
<html>
 <head>
 	<title>LMS-ADMIN</title>
 	 <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
 </head>
 <body>
 	<div class="navbar navbar-dark bg-dark">
</div>
	<div class="container" style="padding-top: 10px;">
		<div class="row">
			<div class="col-md-12">
				 <?php
				 $success = $this->session->flashdata('success');
				 if($success != ""){
				 ?>
				 <div class="alert alert-success"><?php echo $success;?></div>
				 <?php
				}
				?>
				<?php
				 $failure = $this->session->flashdata('failure');
				 if($failure != ""){
				 ?>
				 <div class="alert alert-success"><?php echo $failure;?></div>
				 <?php
				}
				?>
			</div>
		</div>
		 <div class="row">
		 	<div class="col-md-8">
		 		<div class="row">
		 			<div class="col-6"><h3>MANAGE-CATEGORY</h3></div>
					<div class="col-6 text-right">
						<a href="<?php echo base_url().'index.php/Category/create';?>" class="btn btn-primary"><i class="fas fa-plus"></i> ADD</a>
					</div>
		 		</div>
		 		<hr>
		 	</div>
		 </div>

		<div class="row">

			<div class="col-md-8">
				<table class="table table-striped">
					<tr>
						<th>ID</th>
						<th>Category Name</th>
            <th>Status</th>
						<th>Edit</th>
						<th>Delete</th>

					</tr>

					<?php if(!empty($categorys)) { foreach($categorys as $categorys) {?>
						<tr>
							<td><?php echo $categorys['category_id']?></td>
							<td><?php echo $categorys['category_name']?></td>
              <td><?php echo $categorys['status']?></td>
							<td>
								<a href="<?php echo base_url().'index.php/Category/edit/'.$categorys['category_id']?>" class="btn btn-primary btn-sm"><i class="far fa-edit"></i> Edit</a>
							</td>
							<td>
								<a href="<?php echo base_url().'index.php/Category/delete/'.$categorys['category_id']?>" class="btn btn-danger btn-sm"> <i class="far fa-trash-alt"></i> Delete</a>
							</td>
						</tr>
					<?php }} else { ?>
						<tr>
							<td colspan="5">Records not found</td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</body>
 </html>
