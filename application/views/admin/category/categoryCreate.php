<?php $this->load->view('admin/basic');?>
<html>
 <head>
 	<title>LMS-ADMIN</title>
 	 <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
 </head>
 <body>
 	<div class="navbar navbar-dark bg-dark">
 	
</div>
	<div class="container" style="padding-top: 10px;">
		<h3>ADD CATEGORY</h3>
    <?php
    $msg = $this->session->flashdata('msg');
    if($msg !=""){
      echo "<div class='alert alert-success'>'$msg'</div>";
    }
  ?>
		<hr>
		<form method="post" name="createCategory" action="<?php echo base_url().'index.php/Category/create'?>">
		<div class="row">
			<div class="card-body">
				<div class="form-group">
					<label>Catergory Name</label>
					<input type="text" name="category_name" value="<?php echo set_value('category_name');?>" class="form-control">
					<?php echo form_error('category_name')?>
				</div>
          <div class="custom-control custom-radio float-left">
              <input class="custom-control-input" value="1" type="radio" id="statusActive" name="status" checked="">
              <label for="statusActive" class="custom-control-label">Active</label>
          </div>

          <div class="custom-control custom-radio float-left ml-3">
                <input class="custom-control-input" value="0" type="radio" id="statusInactive" name="status">
                <label for="statusInactive" class="custom-control-label">Inactive</label>
          </div>
          <br>
          <br>
				<div class="form-group">
					<button class="btn btn-primary">Create</button>
			<a href="<?php echo base_url().'index.php/Category/index'?>" class="btn-secondary btn">Back</a>
				</div>
			</div>

		</div>
		</form>
	</div>
</body>
 </html>
