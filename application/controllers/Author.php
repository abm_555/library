<?php
class Author extends CI_Controller
{
	public function index(){
		$this->load->model('Author_model');
		$authors = $this->Author_model->all();
		$data = array();
		$data['authors'] = $authors;
		$this->load->view('admin/author/authorList',$data);
	}

	public function create(){
		//trim|required
		$this->load->library('form_validation');
		$this->load->model('Author_model');
		$this->form_validation->set_rules('author_name','Author_Name','required');

		if($this->form_validation->run() == false){
			$this->load->view('admin/author/authorCreate');
		}else{

				//Here we will save rec in db
				$formArray = array();
				$formArray['author_name'] = $this->input->post('author_name');
				$this->Author_model->create($formArray);
				$this->session->set_flashdata('Success','Record added Successfully');
				redirect(base_url().'index.php/Author/create');

			}
	}

	public function edit($authorsId)
	{
		$this->load->library('form_validation');
		$this->load->model('Author_model');
		$authors = $this->Author_model->getAuthors($authorsId);
		$data =array();
		$data['authors'] = $authors;

		$this->form_validation->set_rules('author_name','Author_Name','required');

		if($this->form_validation->run() == false)
		{
			$this->load->view('admin/author/authorEdit',$data);
		}
		else
		{
			$formArray = array();
			$formArray['author_name'] = $this->input->post('author_name');
			$this->Author_model->update($authorsId,$formArray);
			$this->session->set_flashdata('Success','Your updated successfully');
				redirect(base_url().'index.php/Author/index');

		}

	}

	public function delete($authorsId){
		$this->load->model('Author_model');
		$authors = $this->Author_model->getAuthors($authorsId);
		if (empty($authors)){
			$this->session->set_flashdata('Failure','Record not found');
				redirect(base_url().'index.php/Author/index');

		}
		$this->Author_model->delete($authorsId);
		$this->session->set_flashdata('Success','Record Deleted');
		redirect(base_url().'index.php/Author/index');
}
}
?>
