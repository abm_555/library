<?php
class Auth extends CI_Controller
{
	/*This function show register page*/
	public function register()
	{
			$this->load->library('form_validation');
			$this->form_validation->set_message('is_unique','Email already exist,please try another');

			$this->form_validation->set_rules('first_name','First Name','required');
			$this->form_validation->set_rules('last_name','Last Name','required');
			$this->form_validation->set_rules('email','Email','required|valid_email|is_unique[users.email]');
			$this->form_validation->set_rules('phone','Phone','required');
			$this->form_validation->set_rules('password','Password','required');

			if($this->form_validation->run() == false){
				$this->load->view('register');
			}else{

				//Here we will save rec in db

				$this->load->model('Auth_model');
				$formArray = array();
				$formArray['first_name'] = $this->input->post('first_name');
				$formArray['last_name'] = $this->input->post('last_name');
				$formArray['email'] = $this->input->post('email');
				$formArray['phone'] = $this->input->post('phone');
				$formArray['password'] =password_hash($this->input->post('password'), PASSWORD_BCRYPT);
				$status=1;
				$this->Auth_model->create($formArray);

				$this->session->set_flashdata('msg','Your account has been created successfully');
				redirect(base_url().'index.php/Auth/register');

			}
	}
	public function login(){
		$this->load->model('Auth_model');

		$this->load->library('form_validation');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('password','Password','required');

		if($this->form_validation->run() == true){
				//No error
				$email = $this->input->post('email');
				$user = $this->Auth_model->checkUser($email);

				if(!empty($user)){
					$password =$this->input->post('password');

					if(password_verify($password,$user['password']) == true){
						$sessArray['id']= $user['id'];
						$sessArray['first_name']= $user['first_name'];
						$sessArray['last_name']= $user['last_name'];
						$sessArray['email']= $user['email'];
						$this->session->set_userdata('user',$sessArray);

						redirect(base_url().'index.php/auth/dashboard');

					}else{
						$this->session->set_flashdata('msg','Either email or password is incorrect');
						redirect(base_url().'index.php/auth/login');
					}

				}else{
					$this->session->set_flashdata('msg','Either email or password is incorrect');
					redirect(base_url().'index.php/auth/login');
				}

			}else{
				$this->load->view('login');
			}
	}
	function dashboard(){
		$this->load->model('Auth_model');
		if($this->Auth_model->authorized() == false){
			$this->session->set_flashdata('msg','Not authorized');
			redirect(base_url().'index.php/auth/login');
		}

		$user = $this->session->userdata('user');
		$data['user'] = $user;
		$this->load->view('user/dashboard', $data);
	}


	function logout(){
		$this->session->unset_userdata('user');
		redirect(base_url().'index.php/auth/login');
	}
	public function addBook(){
		//echo "Hello";
		$this->load->view('addBook');
	}
	public function createBook(){
		echo 'Inside Books';
	}
}
?>
