<?php
class Dashboard extends CI_Controller
{
  public function index(){
  $this->load->model('Admin_Dashboard_Model');
  $totalcount=$this->Admin_Dashboard_Model->totalcount();
  $sevendayscount=$this->Admin_Dashboard_Model->countlastsevendays();
  $thirtydayscount=$this->Admin_Dashboard_Model->countthirtydays();

  $countbook=$this->Admin_Dashboard_Model->countbook();
  $countissue=$this->Admin_Dashboard_Model->countissue();
  $this->load->view('admin/dashboard',['tcount'=>$totalcount,'tsevencount'=>$sevendayscount,'tthirycount'=>$thirtydayscount,'bcount'=>$countbook,'icount'=>$countissue]);
}

public function logout(){
$this->session->unset_userdata('uid');
$this->session->sess_destroy();
return redirect('index.php/welcome/index');
}

}
?>
