<?php
class Issue extends CI_Controller
{
	public function index(){
		$this->load->model('Issue_model');
		$issues = $this->Issue_model->all();
		$data = array();
		$data['issues'] = $issues;
		$this->load->view('admin/issue/issueList',$data);
	}

  public function addIssues()
  {
      $this->load->model('Issue_model');
      $books = $this->Issue_model->getBook();
      $users= $this->Issue_model->getUser();
      $this->load->view('admin/issue/issueCreate',['books'=>$books, 'users'=>$users]);
  }

  public function createIssue(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('book_name','Book Name','required');
	  $this->form_validation->set_rules('student_name','Student Name','required');

		if($this->form_validation->run()){


			$formArray =$this->input->post();
			$formArray['book_name'] = $this->input->post('book_name');
      $formArray['student_name'] = $this->input->post('student_name');
			$this->load->model('Issue_model');
			if($this->Issue_model->create($formArray))
			{
				$this->session->set_flashdata('msg','Record added Successfully');
			}
			else{
					$this->session->set_flashdata('msg','Record added Successfully');

			}
			return redirect(base_url().'index.php/Issue/addIssues');

		}
		else{
			$this->addIssues();
		}


	}
  public function delete($issuesId)
	{
		$this->load->model('Issue_model');
		$issues = $this->Issue_model->Issues($issuesId);
		if (empty($issues)){
			$this->session->set_flashdata('Failure','Record not found');
				redirect(base_url().'index.php/Issue/index');

		}
		$this->Issue_model->delete($issuesId);
		$this->session->set_flashdata('Success','Record Deleted');
		redirect(base_url().'index.php/Issue/index');
	}
}
  ?>
