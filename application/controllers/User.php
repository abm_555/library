<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class User extends CI_Controller {

//This function is for user signup

public function register()
{
$this->form_validation->set_rules('first_name','First Name','required|alpha');
$this->form_validation->set_rules('last_name','Last  Name','required|alpha');
$this->form_validation->set_rules('email','Email id','required|valid_email|is_unique[users.email]');
$this->form_validation->set_rules('phone','Mobile Number','required|numeric|exact_length[10]');
$this->form_validation->set_rules('password','Password','required|min_length[6]');
$this->form_validation->set_rules('confirmpassword','Confirm Password','required|min_length[6]|matches[password]');
if($this->form_validation->run()){
$first_name=$this->input->post('first_name');
$last_name=$this->input->post('last_name');
$email=$this->input->post('email');
$phone=$this->input->post('phone');
$password=$this->input->post('password');
$status=1;
$this->load->model('Signup_Model');
$this->Signup_Model->insert($first_name,$last_name,$email,$phone,$password,$status);
} else {
$this->load->view('user/signup');
}

}

public function login(){
$this->form_validation->set_rules('email','Email id','required|valid_email');
$this->form_validation->set_rules('password','Password','required');
if($this->form_validation->run()){
$email=$this->input->post('email');
$password=$this->input->post('password');
$status=1;
$this->load->model('User_Login_Model');
$validate=$this->User_Login_Model->validatelogin($email,$password,$status);
if($validate)
{
$this->session->set_userdata('uid',$validate);
return redirect('index.php/Udashboard/index');
} else{
$this->session->set_flashdata('error', 'Invalid details. Please try again with valid details');
redirect('index.php/user/login');

}

} else {
$this->load->view('user/login');
}
}

//function for logout
public function logout(){
$this->session->unset_userdata('uid');
$this->session->sess_destroy();
return redirect('index.php/user/login');
}

}
