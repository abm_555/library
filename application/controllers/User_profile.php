<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class User_profile extends CI_Controller {
function __construct(){
parent::__construct();
if(! $this->session->userdata('uid'))
redirect('index.php/auth/login');
$this->load->model('User_Profile_Model');
}

public function index(){
$userid = $this->session->userdata('uid');
$profiledetails=$this->User_Profile_Model->getprofile($userid);
$this->load->view('user/user_profile',['profile'=>$profiledetails]);
}


public function updateprofile(){
$this->form_validation->set_rules('first_name','First Name','required|alpha');
$this->form_validation->set_rules('last_name','Last Name','required|alpha');
$this->form_validation->set_rules('phone','Mobile Number','required|numeric|exact_length[10]');
if($this->form_validation->run()){
$first_name=$this->input->post('first_name');
$last_name=$this->input->post('last_name');
$phone=$this->input->post('phone');
$userid = $this->session->userdata('uid');
$this->User_Profile_Model->update_profile($first_name,$last_name,$phone,$userid);
$this->session->set_flashdata('success','Profile updated successfull.');
return redirect('index.php/user_profile/index');

} else {
	$this->session->set_flashdata('error', 'Something went wrong. Please try again with valid format.');
redirect('index.php/user/index');
}

}
}
