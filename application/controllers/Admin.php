<?php
class Admin extends CI_Controller
{
		public function register()
	{
			$this->load->library('form_validation');
			$this->form_validation->set_message('is_unique','Email already exist,please try another');
			$this->form_validation->set_rules('email','Email','required|valid_email|is_unique[admin.email]');
			$this->form_validation->set_rules('password','Password','required');

			if($this->form_validation->run() == false){
				$this->load->view('register');
			}else{

				//Here we will save rec in db

				$this->load->model('Auth_model');
				$formArray = array();
				$formArray['email'] = $this->input->post('email');
				$formArray['password'] =password_hash($this->input->post('password'), PASSWORD_BCRYPT);
				$this->Auth_model->create($formArray);

				$this->session->set_flashdata('msg','Your account has been created successfully');
				redirect(base_url().'index.php/Admin/register');

			}
	}
	public function login(){
	$this->load->model('Auth_model');
	
	$this->load->library('form_validation');
	$this->form_validation->set_rules('email','Email','required|valid_email');
	$this->form_validation->set_rules('password','Password','required');

	if($this->form_validation->run() == true){
			//No error
			$email = $this->input->post('email');
			$admin = $this->Auth_model->checkUser($email);

			if(!empty($admin)){
				$password =$this->input->post('password');

				if(password_verify($password,$admin['password']) == true){
					$sessArray['id']= $admin['id'];
					$sessArray['email']= $admin['email'];
					$this->session->set_userdata('admin',$sessArray);

					redirect(base_url().'index.php/dashboard/index');

				}else{
					$this->session->set_flashdata('msg','Either email or password is incorrect');
					redirect(base_url().'index.php/admin/login');
				}

			}else{
				$this->session->set_flashdata('msg','Either email or password is incorrect');
				redirect(base_url().'index.php/admin/login');
			}

		}else{
			$this->load->view('login');
		}
	}
}
?>
