<?php
class Uview extends CI_Controller
{
  public function index(){
		$this->load->model('Uview_model');
		$users = $this->Uview_model->all();
		$data = array();
		$data['users'] = $users;
		$this->load->view('admin/uview/userList',$data);
	}
  public function create(){
		//trim|required
		$this->load->library('form_validation');
		$this->load->model('Uview_model');
		$this->form_validation->set_rules('first_name','First_Name','required');
    	$this->form_validation->set_rules('last_name','Last_Name','required');
   		$this->form_validation->set_rules('email','Email','required');
    	$this->form_validation->set_rules('password','Password','required');
    	$this->form_validation->set_rules('phone','Phone','required');

		if($this->form_validation->run() == false){
			$this->load->view('admin/uview/userCreate');
		}else{

				//Here we will save rec in db
				$formArray = array();
        		$first_name=$this->input->post('first_name');
		        $last_name=$this->input->post('last_name');
		        $email=$this->input->post('email');
		        $phone=$this->input->post('phone');
		        $password=$this->input->post('password');
				$this->Uview_model->create($formArray);
				$this->session->set_flashdata('Success','Record added Successfully');
				redirect(base_url().'index.php/Uview/create');

			}
	}

	public function edit($usersId)
	{
		$this->load->library('form_validation');
		$this->load->model('Uview_model');
		$users = $this->Uview_model->getUsers($usersId);
		$data =array();
		$data['users'] = $users;

	    $this->form_validation->set_rules('first_name','First_Name','required');
	    $this->form_validation->set_rules('last_name','Last_Name','required');
	    $this->form_validation->set_rules('email','Email','required');
	    $this->form_validation->set_rules('password','Password','required');
	    $this->form_validation->set_rules('phone','Phone','required');

		if($this->form_validation->run() == false)
		{
			$this->load->view('admin/uview/userEdit',$data);
		}
		else
		{
			$formArray = array();
      		$first_name=$this->input->post('first_name');
      		$last_name=$this->input->post('last_name');
      		$email=$this->input->post('email');
      		$phone=$this->input->post('phone');
      		$password=$this->input->post('password');
    			$this->Uview_model->update($usersId,$formArray);
    			$this->session->set_flashdata('Success','Your updated successfully');
    				redirect(base_url().'index.php/Uview/index');

		}

	}

	public function delete($usersId){
		$this->load->model('Uview_model');
		$users = $this->Uview_model->getUsers($usersId);
		if (empty($users)){
			$this->session->set_flashdata('Failure','Record not found');
				redirect(base_url().'index.php/Uview/index');

		}
		$this->Uview_model->delete($usersId);
		$this->session->set_flashdata('Success','Record Deleted');
		redirect(base_url().'index.php/Uview/index');
}
}
?>
