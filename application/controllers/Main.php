<?php
class Main extends CI_Controller
{
	public function index(){
		$this->load->model('Main_model');
		$books = $this->Main_model->all();
		$data = array();
		$data['books'] = $books;
		$this->load->view('admin/book/bookList',$data);
	}
	//function to call the category and author
	public function addBooks()
	{
  		$this->load->model('Main_model');
  		$cates = $this->Main_model->getCategory();
  		$auths= $this->Main_model->getAuthor();
  		$this->load->view('admin/book/bookCreate',['cates'=>$cates, 'auths'=>$auths]);
	}
	
	public function createBook(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('book_name','Book Name','required');
		$this->form_validation->set_rules('cat_id','Category Name','required');
		$this->form_validation->set_rules('auth_id','Author Name','required');

		if($this->form_validation->run()){


			$formArray =$this->input->post();
			$formArray['book_name'] = $this->input->post('book_name');
			$formArray['cat_id'] = $this->input->post('cat_id');
			$formArray['auth_id'] = $this->input->post('auth_id');
			$this->load->model('Main_model');
			if($this->Main_model->create($formArray))
			{
				$this->session->set_flashdata('msg','Record added Successfully');
			}
			else{
					$this->session->set_flashdata('msg','Record added Successfully');

			}
			return redirect(base_url().'index.php/Main/addBooks');

		}
		else{
			$this->addBooks();
		}


	}


	public function delete($booksId)
	{
		$this->load->model('Main_model');
		$books = $this->Main_model->getBooks($booksId);
		if (empty($books)){
			$this->session->set_flashdata('Failure','Record not found');
				redirect(base_url().'index.php/Main/index');

		}
		$this->Main_model->delete($booksId);
		$this->session->set_flashdata('Success','Record Deleted');
		redirect(base_url().'index.php/Main/index');
	}
		public function editBooks($booksId)
		{
			$this->load->model('Main_model');
			$cates = $this->Main_model->getCategory();
			$auths= $this->Main_model->getAuthor();
			$books = $this->Main_model->getBookRecord($booksId);
			$this->load->view('admin/book/bookEdit',['cates'=>$cates, 'auths'=>$auths,'books' => $books]);
		}
		public function modifyBooks($booksId)
		{
			//echo $booksId;(Test)
			$this->load->library('form_validation');
			$this->form_validation->set_rules('book_name','Book Name','required');
			$this->form_validation->set_rules('cat_id','Category Name','required');
			$this->form_validation->set_rules('auth_id','Author Name','required');

			if($this->form_validation->run()){


				$formArray =$this->input->post();
				$formArray['book_name'] = $this->input->post('book_name');
				$formArray['cat_id'] = $this->input->post('cat_id');
				$formArray['auth_id'] = $this->input->post('auth_id');
				$this->load->model('Main_model');
				if($this->Main_model->updateBook($formArray,$booksId))
				{
					$this->session->set_flashdata('msg','Record added Successfully');
				}
				else{
						$this->session->set_flashdata('msg','Record added Successfully');

				}
				return redirect(base_url().'index.php/Main/editBooks/{$booksId}');

			}
		else{
			$this->editBooks();
		}

}
}
?>
