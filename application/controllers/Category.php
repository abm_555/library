<?php
class Category extends CI_Controller
{
	public function index(){
		$this->load->model('Category_model');
		$categorys = $this->Category_model->all();
		$data = array();
		$data['categorys'] = $categorys;
		$this->load->view('admin/category/categoryList',$data);
	}

	public function create(){
		//trim|required
		$this->load->library('form_validation');
		$this->load->model('Category_model');
		$this->form_validation->set_rules('category_name','Category_Name','required');
    	$this->form_validation->set_rules('status','Status','required');
		if($this->form_validation->run() == false){
			$this->load->view('admin/category/categoryCreate');
		}else{

				//Here we will save rec in db
				$formArray = array();
				$formArray['category_name'] = $this->input->post('category_name');
        $formArray['status'] = $this->input->post('status');
				$this->Category_model->create($formArray);
				$this->session->set_flashdata('Success','Record added Successfully');
				redirect(base_url().'index.php/Category/create');

			}
	}

	public function edit($categorysId)
	{
		$this->load->library('form_validation');
		$this->load->model('Category_model');
		$categorys = $this->Category_model->getCategorys($categorysId);
		$data =array();
		$data['categorys'] = $categorys;

		$this->form_validation->set_rules('category_name','Book_Name','required');
    	$this->form_validation->set_rules('status','Status','required');

		if($this->form_validation->run() == false)
		{
			$this->load->view('admin/category/categoryEdit',$data);
		}
		else
		{
			$formArray = array();
			$formArray['category_name'] = $this->input->post('category_name');
      $formArray['status'] = $this->input->post('status');
			$this->Category_model->update($categorysId,$formArray);
			$this->session->set_flashdata('Success','Your updated successfully');
				redirect(base_url().'index.php/Category/index');

		}

	}

	public function delete($categorysId){
		$this->load->model('Category_model');
		$categorys = $this->Category_model->getCategorys($categorysId);
		if (empty($categorys)){
			$this->session->set_flashdata('Failure','Record not found');
				redirect(base_url().'index.php/Category/index');

		}
		$this->Category_model->delete($categorysId);
		$this->session->set_flashdata('Success','Record Deleted');
		redirect(base_url().'index.php/Category/index');
}
}
?>
