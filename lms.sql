-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2020 at 06:05 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(1, 'anoopbenny2017@gmail.com', '$2y$10$QF4BX.0gO89UJnh68YN9eO3JDG.TUIeQNGtGo3/WhQ6j0oo3IUsRm'),
(2, 'samjo@gmail.com', '$2y$10$gxrRHvMB/JWUSofEzVYW6OiuLgEr7nYG.6Na7u4H3uovI8L0Vk9Wq'),
(3, 'john23@gmail.com', '$2y$10$gxOEDLjSWgv5Ak1qD3aBJ.haRv6f4NMN6/JpX9itwuzjUJKZaeLOO'),
(4, 'benny95@gmail.com', '$2y$10$H4cD6m.M2rd.S00NZWx2DescU7SRwPqG3v4sjrqlate0b5.yoyDqq'),
(5, 'rony@gmail.com', '$2y$10$d.TZB51srCwNWXRo5gdTVuPRICvNjJjUihQBrIis8FqyJ4fOg16x2'),
(6, 'admin@gmail.com', '$2y$10$XajdC7f8f6Q9pbpxY2Mc3Oj79lejfTtPPke50AqE6tAFRAnAvPi2i');

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `author_id` int(11) NOT NULL,
  `author_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`author_id`, `author_name`) VALUES
(1, 'Sam John'),
(2, 'John'),
(5, 'Tony'),
(9, 'Thara');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(11) NOT NULL,
  `book_name` varchar(100) NOT NULL,
  `cat_id` int(100) NOT NULL,
  `auth_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `book_name`, `cat_id`, `auth_id`) VALUES
(1, 'Romeo', 2, 2),
(4, 'Titanic', 1, 2),
(5, 'Test 2', 2, 5),
(6, 'Rome', 1, 1),
(7, 'Rambo', 5, 7),
(9, 'Titanic2', 1, 5),
(11, 'Solar System', 6, 8),
(12, 'Summer', 1, 2),
(13, 'Accounting', 3, 5),
(14, 'Naturals Theory', 10, 9);

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`category_id`, `category_name`, `status`) VALUES
(1, 'Romantic', '1'),
(2, 'Thrillers', '1'),
(3, 'Science', '1'),
(7, 'tes3', '1'),
(9, 'Banking', '1'),
(10, 'Natural Science', '1'),
(11, 'CS', '1');

-- --------------------------------------------------------

--
-- Table structure for table `issues`
--

CREATE TABLE `issues` (
  `issue_id` int(11) NOT NULL,
  `book_name` varchar(100) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `isuuseDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issues`
--

INSERT INTO `issues` (`issue_id`, `book_name`, `student_name`, `isuuseDate`) VALUES
(1, 'Romeo', 'Anoop', '2020-07-11 12:12:21'),
(2, 'Titanic', 'Ram', '2020-07-11 12:12:52'),
(3, 'Titanic', 'Ram', '2020-07-11 12:18:51'),
(4, 'Rome', 'Tharamma', '2020-07-14 12:15:20'),
(5, 'Naturals', 'Tharamma', '2020-07-14 12:45:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `regDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `isActive` int(1) NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `regDate`, `isActive`, `updated_at`) VALUES
(3, 'Anoop', 'Benny', 'anoopbenny2017@gmail.com', '123456', '9538384058', '2020-07-11 16:51:15', 1, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`author_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `issues`
--
ALTER TABLE `issues`
  ADD PRIMARY KEY (`issue_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `author_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `issues`
--
ALTER TABLE `issues`
  MODIFY `issue_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
